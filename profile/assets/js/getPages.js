var spinner = `<div class="d-flex justify-content-center">
    <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
    </div>
</div>`

var admissionTab = document.querySelector('#admissionTab')
var profileTab = document.querySelector('#profileTab')
var paymentTab = document.querySelector('#paymentTab')
var showContent = document.querySelector("#basicInfo")
var storageData = localStorage.getItem('token');
window.addEventListener('load', {
    handleEvent: function (event) {
        
    fetch("../profile/includes/profile.html")
    .then(response => {
        return response.text()
    })
    .then(profileData => {
      let storageData = localStorage.getItem('token');

        axios.get('https://tnx-college.herokuapp.com/login',{

            headers: {"Authorization" : `Bearer ${storageData}`}
               
            })
           .then(function (response) {
            //  render data on the profile page
           var userData = response.data
           console.log(userData)
            
           var fullName = document.querySelector('#fullName1')
           var email = document.querySelector('#email1')
           var phone = document.querySelector('#phone1')
           var city = document.querySelector('#city1')

           fullName.value = userData.fullName
           email.value = userData.email
           phone.value = userData.phone
           city.value  = userData.city
         
          }).catch(function (error) {
          console.log(error.response.data.status)
          window.location = "../profile/login/"
    
            return Promise.reject(error.response);
            });
        document.querySelector('#basicInfo').innerHTML = profileData
    })
    
       
    
profileTab.addEventListener('click', () => 
    fetch("../profile/includes/profile.html")
    .then(response => {
        return response.text()
    })
    .then(profileData => {
       

        axios.get('https://tnx-college.herokuapp.com/login',{

            headers: {"Authorization" : `Bearer ${storageData}`}
               
            })
           .then(function (response) {
            //  render data on the profile page
           var userData = response.data
           console.log(userData)
            
           var fullName = document.querySelector('#fullName1')
           var email = document.querySelector('#email1')
           var phone = document.querySelector('#phone1')
           var city = document.querySelector('#city1')

           fullName.value = userData.fullName
           email.value = userData.email
           phone.value = userData.phone
           city.value  = userData.city
         
          }).catch(function (error) {
          console.log(error.response.data.status)
          window.location = "../profile/login/"
    
            return Promise.reject(error.response);
            });
        document.querySelector('#basicInfo').innerHTML = profileData
        // showContent.innerHTML = profileData
    })
)

admissionTab.addEventListener('click', () => 
    fetch("../profile/includes/admission.html")
    .then(response => {
        return response.text()
    })
    .then(admissionData => {
        showContent.innerHTML = admissionData
      

      axios.get('https://tnx-college.herokuapp.com/login',{

        headers: {"Authorization" : `Bearer ${storageData}`}
           
        })
       .then(function (response) {
        //  render data on the profile page
       var userData = response.data
       console.log(userData)
      
        var pack = document.querySelector('#pack')
        var title = document.querySelector('#title')
     
       
        var regAt = document.querySelector('#regAt1')
        var tuitionFee = document.querySelector('#tuitionFee')
        var tuitionFeeStatus = document.querySelector('#tuitionFeeStatus')
        var regFee = document.querySelector('#regFee')
        var regFeeStatus = document.querySelector('#regFeeStatus')
        var admissionMsg = document.querySelector('#admissionMsg')

        var regNo = document.querySelector('#regNo1')
        var downloadLetter = document.querySelector('#downloadLetter1')
        var admissionStatus = document.querySelector('#admissionStatusDom')


        var admissionInfo = {
            'status': userData.isAdmitted,
            'admissionMsgFalse' : 'Your Admission later is currently not available',
            'admissionMsgTrue' : 'you are successfully Admitted',
          'adInfo' : {
            'course' : userData.course.title,
            'regNo' : userData.regNumber,
            'regAt' : new Date(userData.createdAt).toDateString(),
            'tuitionFee' : userData.payment.coursePrice,
            'tutionFeeStatus' : userData.tuitionFeeStatus,
            'regFee': userData.payment.applicationFee,
            'regFeeStatus' : userData.applicationpPaymentStatus,
            'pack' : userData.selectedPackage,
    
          },
          'adStatus' :{

          },
          'studentGuide' :{

          }

        }

        // Admission DOM

      
        
        
        pack.innerHTML = admissionInfo.adInfo.pack
        title.innerHTML = admissionInfo.adInfo.course

        regNo.innerHTML = admissionInfo.adInfo.regNo
        // regNo.innerHTML = admissionData.adInfo.regNo
        regAt.innerHTML = admissionInfo.adInfo.regAt
        tuitionFee.innerHTML = admissionInfo.adInfo.tuitionFee
        tuitionFeeStatus.innerHTML = admissionInfo.adInfo.tutionFeeStatus
        regFee.innerHTML = admissionInfo.adInfo.regFee
        regFeeStatus.innerHTML = admissionInfo.adInfo.regFeeStatus


        if (admissionInfo.status == true) {
            admissionStatusDom.innerHTML = 'Admitted'
            downloadLetter.style.display = 'block'
            admissionMsg.innerHTML = admissionInfo.admissionMsgTrue
            downloadLetter.addEventListener('click', function() {
                window.open("/profile/assets/admission-letters/" + admissionInfo.adInfo.regNo + ".pdf", "_blank");
            })
        }else{
            admissionStatus.innerHTML = "Admission in Progress"
            admissionStatus.style.color="red"
            admissionMsg.innerHTML = admissionInfo.admissionMsgFalse
            admissionMsg.style.color="red"
          }
    
      }).catch(function (error) {
      console.log(error.response.data.status)
      window.location = "../profile/login/"

        return Promise.reject(error.response);
        });
    })
)

paymentTab.addEventListener('click', () => 
    fetch("../profile/includes/payment.html")
    .then(response => {
        return response.text()

    })
    .then(paymentData => {
        showContent.innerHTML = paymentData
        let storageData = localStorage.getItem('token');

        axios.get('https://tnx-college.herokuapp.com/login',{

        headers: {"Authorization" : `Bearer ${storageData}`}
        })
       .then(function (response) {
        //  render data on the profile page
       var userData = response.data
       console.log(userData)
       var paymentInfo = {
        'applicationFeeStatus': userData.applicationpPaymentStatus,
        'tutionFeeStatus' : userData.tuitionFeeStatus,
        'invoice':{
          'id' : userData.payment.invoice
        },
        'receipt':{
        },


      }
        var downloadInvoice = document.querySelector('#downloadInvoice')
        var selectedIntallment = document.querySelector('#selectedIntallment')
        // console.log(userInfo.isAdmitted)
        let userId = userData._id;
            queryString = "?userId=" + userId;
        downloadInvoice.addEventListener('click', getInvoice)
         function getInvoice() {
             if (userData.applicationFeeStatus == 'PAID') {
                 alert('Already paid')
             }else{
                window.open("../apply/invoice/" + queryString, "_blank");

             }
        }
        
        // let redirectUrl = "../apply/invoice/" + queryString
        proceedToInvoice.addEventListener('click', function() {
          window.open("../profile/tuitionfee" + queryString + "&installment=" + selectedIntallment.value, "_blank");
        })
       
        // console.log(response.data.accessCode);
    
      }).catch(function (error) {
      console.log(error.response.data.status)
      window.location = "../profile/login/"

      return Promise.reject(error.response);
      });

    })
)
classroomTab.addEventListener('click', () => 
    fetch("../profile/includes/classroom.html")
    .then(response => {
        return response.text()
    })
    .then(classroomData => {
        showContent.innerHTML = classroomData

    })
)
iboTab.addEventListener('click', () => 
    fetch("../profile/includes/ibo.html")
    .then(response => {
        return response.text()
    })
    .then(iboData => {
        showContent.innerHTML = iboData

        // call user data start
        axios.get('https://tnx-college.herokuapp.com/login',{

            headers: {"Authorization" : `Bearer ${storageData}`}
               
            })
           .then(function (response) {
            var submitSms = document.querySelector('#submitSms')
            var smsPhone = document.querySelector('#smsPhone')
            var smsName = document.querySelector('#smsName')
            submitSms.addEventListener('click', () => {
                axios({
                    method: 'POST',
                    header: {
                        crossDomain:"true",
                        "Access-Control-Allow-Origin": "*",
                    },
                    url: 'https://www.bulksmsnigeria.com/api/v1/sms/create',
                    data:{
                        "api_token" : "xNw3PxnUtYbHDlPQxgwiQufaswSIMrLDfuApdCAlTtN6yXUgUlyYKU2TUUH0",
                        "from": "TNX-COLLEGE",
                        "to": smsPhone.value,
                        "body": "Hello, " + `${smsName.value}` +  "its  " + `${response.data.fullName}` +  " I am encouraging you to enroll and take advantage of the TNX College's core8 programme by joining millions of others to learn a skill set kindly tab the following link to get registered or contact +2348076974141, for more enquiries"
                    },
                    })
                    .then(function (response) {
                    console.log(response)
                
                    }).catch(function (error) {
                    window.location.reload(true)
                    return Promise.reject(error.response)
                    });
            })
           })
        // call user data stop
        // send sms
        
                

    })
)

    fetch("../profile/includes/noticeBoard.html")
    .then(response => {
    return response.text()
    })
    .then(noticeBoardData => {
    document.querySelector("#noticeBoard").innerHTML = noticeBoardData;
    });
    }
})




// var accountTab = document.querySelector('#accountTab')


var checkIfExist = document.querySelector('#checkIfExist')
var resetEmail = document.querySelector('#resetEmail')
var resetCode = document.querySelector('#resetCode')
var enterResetCode = document.querySelector('#enterResetCode')

var varificationCode = document.querySelector('#varificationCode')
var newAccessCode = document.querySelector('#newAccessCode')
var rspMsg = document.querySelector('#rspMsg')

checkIfExist.addEventListener('click', () => {
    axios({
        header:{
            crossDomain:"true",
            "Access-Control-Allow-Origin": "*",
        },
        method:'POST',
        url: 'https://tnx-college.herokuapp.com/send-email',
        data:{
            'email' : resetEmail.value
        },
    })
    .then((response) => {
        console.log(response.data)
        var msg = response.data.message
       
        rspMsg.innerHTML = `<div class="alert alert-success" role="alert">
        <h6 class="alert-heading">Done!</h6>
        <small>${msg}</small>
        <hr>
        
      </div>`
      resetEmail.style.display="none"
      enterResetCode.style.display="block"
      checkIfExist.style.display="none"
      varificationCode.style.display="block"
      newAccessCode.style.display="block"



    
    })
    .catch((response) => {
        console.log(response.error)
    })
})

enterResetCode.addEventListener('click', () => {
    axios({
        header:{
            crossDomain:"true",
            "Access-Control-Allow-Origin": "*",
        },
        method:'POST',
        url: 'https://tnx-college.herokuapp.com/reset-access-code',
        data:{
            "verificationCode":varificationCode.value,
            "newAccessCode":newAccessCode.value,
        },
    })
    .then((response) => {
        var msg = response.data.message
        var rspMsg = document.querySelector('#rspMsg')
        rspMsg.innerHTML = `<div class="alert alert-success" role="alert">
        <h6 class="alert-heading">Done!</h6>
        <small>${msg}</small>
        <hr>
        <small class="text-muted">(c)TNXCOLLEGE)</small>
      </div>`
      resetEmail.style.display="none"
    })
    .catch((response) => {
        console.log(response.error)
    })
})

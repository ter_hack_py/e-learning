var totalFee = document.querySelector('#totalFee').innerHTML
var advancedFull = [
      {"id":1, "item":"tuitionFee", "amount": 40000},
      {"id":2, "item":"portal", "amount": 2800},
      {"id":3, "item":"internet", "amount": 4000},
      {"id":4, "item":"idCard", "amount": 500},
      {"id":5, "item":"documentation", "amount": 2800},
      {"id":6, "item":"sanitation", "amount": 2700},
      {"id":7, "item":"utility", "amount": 4800},
      {"id":8, "item":"payment", "amount": 1900},
      {"id":9, "item":"libary", "amount": 5700},
      {"id":9, "item":"practical", "amount": 4700},
      {"id":10, "item":"softwares", "amount": 3700},
      {"id":11, "item":"damages", "amount": 3300}
  ]
var intermediateFee = [
    {"id":1, "item":"tuitionFee", "amount": 29050},
    {"id":2, "item":"portal", "amount": 2800},
    {"id":3, "item":"internet", "amount": 1000},
    {"id":4, "item":"idCard", "amount": 500},
    {"id":5, "item":"documentation", "amount": 2800},
    {"id":6, "item":"sanitation", "amount": 1500},
    {"id":7, "item":"utility", "amount": 2000},
    {"id":8, "item":"payment", "amount": 850},
    {"id":9, "item":"libary", "amount": 2000},
    {"id":10, "item":"softwares", "amount": 2000},
    {"id":9, "item":"practical", "amount": 4700},
    {"id":11, "item":"damages", "amount": 2000}
]
var bassicFee = [
    {"id":1, "item":"tuitionFee", "amount": 15000},
    {"id":2, "item":"portal", "amount": 2800},
    {"id":3, "item":"internet", "amount": 500},
    {"id":4, "item":"idCard", "amount": 500},
    {"id":5, "item":"documentation", "amount": 2800},
    {"id":6, "item":"sanitation", "amount": 250},
    {"id":7, "item":"utility", "amount": 500},
    {"id":8, "item":"payment", "amount": 475},
    {"id":9, "item":"libary", "amount": 875},
    {"id":10, "item":"softwares", "amount": 550},
    {"id":9, "item":"practical", "amount": 550},
    {"id":11, "item":"damages", "amount": 550}
]

  var advancedPart = [
    {"id":1, "item":"tuitionFee", "amount": 39500}
  ]
  
  var outputAdvancedFull = advancedFull.map((i) => {
    return `
        <tr>
            <td><span>${i.id}</span></td>
            <td><span data-prefix>${i.item}</span><span></span></span></td>
            <td><span data-prefix>${i.amount}</span><span id="userPrice"></span></td>
        </tr>
             
            `;
  });
  var outputAdvancedPart = advancedPart.map((i) => {
    return `
        <tr>
            <td><span>${i.id}</span></td>
            <td><span data-prefix>${i.item}</span><span></span></span></td>
            <td><span data-prefix>${i.amount}</span><span id="userPrice"></span></td>
        </tr>
             
            `;
  });
  var outputIntermediate = intermediateFee.map((i) => {
    return `
        <tr>
            <td><span>${i.id}</span></td>
            <td><span data-prefix>${i.item}</span><span></span></span></td>
            <td><span data-prefix>${i.amount}</span><span id="userPrice"></span></td>
        </tr>
            `
  })

  var outputBasic = bassicFee.map((i) => {
    return `
        <tr>
            <td><span>${i.id}</span></td>
            <td><span data-prefix>${i.item}</span><span></span></span></td>
            <td><span data-prefix>${i.amount}</span><span id="userPrice"></span></td>
        </tr>
             
            `;
  });
  
//   console.log(output);
  var inventory = document.querySelector('#inventoryDisplay')
            console.log(outputIntermediate)

 
// let allItems = new Array(invoiceContent)
// console.log(allItems[0].tutionFeeFull)

download.addEventListener('click', function() {
    window.print();
});

var urlParams = new URLSearchParams(window.location.search);
if (urlParams.get('userId') == "" ) {
    window.location = "../";
}


// userName.innerHTML = urlParams.get('fullName');
// userEmail.innerHTML = urlParams.get('email');
// payEmail.value = urlParams.get('email');
// console.log(urlParams.has('email'))
// console.log(urlParams.has('amount')); 
// console.log(urlParams.get('email')); 
// console.log(urlParams.getAll('user'));
// console.log(urlParams.toString()); 
// console.log(urlParams.append('user', '1')); 

urlParams.get('userId');
window.addEventListener('load', {
    handleEvent: function (event) {
      event.preventDefault();
      axios({
        method: 'GET',
        url: 'https://tnx-college.herokuapp.com/apply/'+ urlParams.get('userId'),
        // data:{
            // 'adminKey':'metsys525'
        // },
        
      })


       .then(function (response) {
        // console.log(response.data);

        // var result = response.data
        if (response.data.selectedPackage == 'Intermediate') {
            inventory.innerHTML = outputIntermediate
            totalFee = 50000
        }else if (response.data.selectedPackage == 'Advanced' && urlParams.get('installment') == 2 ) {
            inventory.innerHTML = outputAdvancedFull
            totalFee = 120000

        }else{
            inventory.innerHTML = outputAdvancedPart
            totalFee = 39500

        }
        if (response.data.selectedPackage == 'Basic') {
            inventory.innerHTML = outputBasic
            totalFee = 25000
        }
        document.querySelector('#totalFee').innerHTML = totalFee
        if (response.status == 200) {
        let paymentMessage = {
            online: "Your Registration for " + response.data.course.title + " was Successful, " + "<br> "+ "You will Receive your Admission letter within 48 hours and futher instruction to pay your tuition fees.<br> Kindly Save this Invoice or screenshot it for future reference. Thanks",
            bank: "Please kindly use the above invoice number as description when trandforming or making deposit at the, save this invoice, after payment call +2348076974141 or email prove of payment to account@tnxcollege.com",
            school: "Kindly visit the School @13 Nsefik Layout or call +2348076974141 to make your payment using the invoice saved or sent to your email.",
        }
        // console.log(response.data)
        
        applicationpPaymentStatus.innerHTML = response.data.applicationpPaymentStatus;
        paymentMethod.innerHTML = response.data.payment.paymentMethod;
        invoice.innerHTML = response.data.payment.invoice;
        payInvoice.value = response.data.payment.invoice;
        userName.innerHTML = response.data.fullName;
	    userCity.innerHTML = response.data.city;
		userPhone.innerHTML = response.data.phone;
		userEmail.innerHTML = response.data.email;
        userReg.innerHTML = response.data.regNumber;
        
        createdAt.innerHTML = new Date(response.data.createdAt).toDateString();
        // tuitionFeeStatus.innerHTML = response.data.tuitionFeeStatus;
        courseTitle.innerHTML = response.data.course.title;
        userPackage.innerHTML = response.data.selectedPackage;
        userPrice.innerHTML = response.data.payment.applicationFee;

        userAmoountTotal.innerHTML = userPrice.innerHTML;
        payEmail.value = response.data.email;
        payAmount.value = response.data.payment.applicationFee;

       
        // check if the payment status is paid
        if (applicationpPaymentStatus.innerHTML == "PAID") {
            paid.style.display = "flex";
        }
      
        if (paymentMethod.innerHTML == "ONLINE" && applicationpPaymentStatus.innerHTML == "PAID") {
            paid.style.display="block";
        }else{
            if (paymentMethod.innerHTML == "ONLINE" && applicationpPaymentStatus.innerHTML == "NOT PAID") {
                payNow.style.display="inline";
            }
        }
        if (paymentMethod.innerHTML == "SCHOOL" && applicationpPaymentStatus.innerHTML == "NOT PAID") {
            bankName.style.display="block";
            accountName.style.display="block";
            accountNumber.style.display="block";
            instruction.innerHTML = paymentMessage.school
        }
        if (paymentMethod.innerHTML == "BANKDEPOSIT" && applicationpPaymentStatus.innerHTML == "NOT PAID") {
            bankName.style.display="block";
            accountName.style.display="block";
            accountNumber.style.display="block";
            instruction.innerHTML = paymentMessage.bank

        }
        
       
        
        if (paymentMethod.innerHTML == 'BANKDEPOSIT' && applicationpPaymentStatus.innerHTML == "NOT PAID") {
            axios({
                method: 'POST',
                header: {
                    crossDomain:"true",
                    "Access-Control-Allow-Origin": "*",
                },
                url: 'https://www.bulksmsnigeria.com/api/v1/sms/create',
                data:{
                    "api_token" : "xNw3PxnUtYbHDlPQxgwiQufaswSIMrLDfuApdCAlTtN6yXUgUlyYKU2TUUH0",
                    "from": "TNX-COLLEGE",
                    "to": userPhone.innerHTML,
                    "body": "To complete your registration kindly use your invoice id:" + invoice.innerHTML +" as description while transfering or making payment at the bank after transfer or bank deposit call +2348076974141 or Email receipt to accounts@tnxcollege.com"
                },
                })
                .then(function (response) {
                console.log(response)

                }).catch(function (error) {
                // window.location.reload(true)
                // return Promise.reject(error.response)
                });

            
        }
        if (paymentMethod.innerHTML == 'SCHOOL' && applicationpPaymentStatus.innerHTML == "NOT PAID") {
            axios({
                method: 'POST',
                header: {
                    crossDomain:"true",
                    "Access-Control-Allow-Origin": "*",
                },
                url: 'https://www.bulksmsnigeria.com/api/v1/sms/create',
                data:{
                    "api_token" : "xNw3PxnUtYbHDlPQxgwiQufaswSIMrLDfuApdCAlTtN6yXUgUlyYKU2TUUH0",
                    "from": "TNX-COLLEGE",
                    "to": userPhone.innerHTML,
                    "body": "To complete your registration kindly visit the school @13 Nsefik Eyo Layout or call +2348076974141 or Email receipt to accounts@tnxcollege.com"
                },
                })
                .then(function (response) {
                console.log(response)

                }).catch(function (error) {
                // window.location.reload(true)
                // return Promise.reject(error.response)
                });

        }

      }else{
          window.location = "/apply/";
      }
      if (urlParams.get('userId') == "") {
          window.location = "/apply/";
      }
        
      }).catch(function (error) {
      return Promise.reject(error.response);
      });
    }
  });
// console.log(result)
 

 

 






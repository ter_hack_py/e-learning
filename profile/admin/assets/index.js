// const formData = new FormData();
// const fileField = document.querySelector('input[type="file"]');

// formData.append('username', 'abc123');
// formData.append('avatar', fileField.files[0]);

// fetch('https://example.com/profile/avatar', {
//   method: 'POST',
//   body: formData
// })
// .then(response => response.json())
// .then(result => {
//   console.log('Success:', result);
// })
// .catch(error => {
//   console.error('Error:', error);
// });

// === call side nav ==
fetch("../includes/sideNav.html")
  .then(response => {
    return response.text()
  })
  .then(sideNavData => {
    document.querySelector("#sideNav").innerHTML = sideNavData;
  });

google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
        ['Task', 'Hours per Day'],
        ['Registered',     11],
        ['Admiited',      2],
        ['Paid',  2],
        ['Not paid', 2],
        ['All',    7]
        ]);
        var options = {
        title: 'Activity log'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }

    window.addEventListener('load', {
      handleEvent: function (event) {
        event.preventDefault();
        let storageData = localStorage.getItem('token');
        // console.log(storageData)
          // console.log(storageData + "My data")
        axios.get('https://tnx-college.herokuapp.com/login',{
  
          headers: {"Authorization" : `Bearer ${storageData}`}
             
          })
         .then(function (response) {
          //  render data on the profile page
          userData = response.data
          var userName = document.querySelector('#userName')
          userName.innerHTML = userData.fullName
          userEmail.innerHTML = userData.email
          if (userData.role != 'SUPER_USER') {
            window.location = '/../login'
          }
          admitStudent.addEventListener('click', () => {
            axios({
        
              headers: {"Authorization" : `Bearer ${storageData}`},
              method: 'PUT',
              url: 'https://tnx-college.herokuapp.com/admin/5f31176e30a81c0017c56013',
              data: {
                "isAdmitted" : true,
                "phone":"00030303030",
                "paymentStatus": "",

               
              }
            })
        
            .then(function (response) {
        
              // if (request.readyState < 200) {
              //   console.log('Please wait.....')
              // }
              console.log(response)
            }).catch(function (error) {
            console.log(error.response.data.status)
            
            return Promise.reject(error.response);
            });
        
          })
          
          // console.log(response.data)
         
     
        }).catch(function (error) {
        console.log(error.response.data.status)
        window.location = "/../login"
  
        return Promise.reject(error.response);
        });
  
      }
    });


  
    
    // alert('Hello world testing')
// let submitUserData = document.querySelector('#submitUserData')
// submitUserData.addEventListener('click', function() {
//   alert('Working')
  
// })


// let uid = 'Elijah Samuels'
// let user = function createUser() {
//  let user = `<tr>
//     <td>
//       <div class="media align-items-center">
//         <span class="avatar avatar-sm avatar-navy avatar-circle mr-3">
//           <span class="avatar-initials">FH</span>
//         </span>
//         <div class="media-body">
//           <a class="d-inline-block text-dark" href="#">
//             <h6 class="text-hover-primary mb-0">${uid}</h6>
//           </a>
//           <small class="d-block">francishanson@gmil.com</small>
//         </div>
//       </div>
//     </td>
//     <td>
     
//       <select class="js-custom-select"
//             data-hs-select2-options='{
//               "minimumResultsForSearch": "Infinity",
//               "customClass": "custom-select custom-select-sm",
//               "dropdownAutoWidth": true
//             }'>
//         <option value="" selected data-option-template='<span class="d-flex align-items-center"><span class="legend-indicator bg-success mr-2"></span> Admitted</span>'>Admitted</option>
//         <option value="" data-option-template='<span class="d-flex align-items-center"><span class="legend-indicator bg-danger mr-2"></span> Pending</span>'>Pending</option>
//       </select>
//     </td>
//     <td>
//       <select class="js-custom-select"
//             data-hs-select2-options='{
//               "minimumResultsForSearch": "Infinity",
//               "customClass": "custom-select custom-select-sm",
//               "dropdownAutoWidth": true
//             }'>
//         <option value="memberStatusLabelAdmin4">Web Development</option>
//         <option value="memberStatusLabelCanEdit4">Mobile Development</option>
//         <option value="memberStatusLabelCanView4" selected>Graphics</option>
//       </select>
//     </td>
//     <td>
//       <a class="text-body" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Remove member">
//         <i class="far fa-trash-alt"></i>
//       </a>
//     </td>
//   </tr>`
// }
// console.log(user)
// studentWrapper.innerHTML = user



const admit = document.querySelector('#admit')
const filterRegNo = document.querySelector('#filterRegNo')
filterRegNo.addEventListener('input', () => {
    axios({

        headers: {"Authorization" : `Bearer ${storageData}`},
        method: 'PUT',
        url: 'https://tnx-college.herokuapp.com/admin/all-users/filter',
        data: {
            "regNumber": filterRegNo.value
        }
    })
    .then(function (response) {
        console.log(response)
    }).catch(function (error) {
        console.log(error.response.data.status)
        
        return Promise.reject(error.response);
    });
   
})